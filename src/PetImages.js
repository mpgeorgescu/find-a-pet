import React from 'react';
import Thumbnail from './Thumbnail';

class PetImages extends React.Component {
  constructor(props) {
    super(props);
    this.handleThumbNailClick = this.handleThumbNailClick.bind(this);
    this.state = {
      photos: [],
      active: 0,
    };
  }

  static getDerivedStateFromProps({ media }) {
    // filter photos by desired size and put them in the state
    let photos = [];
    if (media && media.photos && media.photos.photo) {
      photos = media.photos.photo.filter(photo => photo['@size'] === 'pn');
    }
    return { photos };
  }

  /**
   * updates the active photo index in the state
   */
  handleThumbNailClick(event) {
    this.setState({
      active: +event.target.dataset.index,
    });
  }

  render() {
    const { photos, active } = this.state;
    return (
      <div className="all-images">
        <img src={photos[active].value} alt="pet {active}" />
        <div className="pet-thumbnails">
          {
            photos.map((photo, index) => (
              <Thumbnail
                handleThumbNailClick={this.handleThumbNailClick}
                key={photo.value}
                photo={photo}
                index={index}
                active={active}
              />
            ))
          }
        </div>
      </div>
    );
  }
}

export default PetImages;
