import React from 'react';
import pf from 'petfinder-client';
import PetImages from './PetImages';

const petfinder = pf({
  key: process.env.API_KEY,
  secret: process.env.API_SECRET,
});

class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    const { id } = this.props;
    petfinder.pet.get({
      output: 'full',
      id,
    }).then((data) => {
      const { pet } = data.petfinder;
      let { breed } = pet.breeds;
      if (Array.isArray(breed)) {
        breed = breed.join('/');
      }
      this.setState({
        xname: pet.name,
        animal: pet.animal,
        location: `${pet.contact.city}, ${pet.contact.state}`,
        description: pet.description,
        media: pet.media,
        breed,
        age: pet.age,
        loading: false,
      });
    }).catch(err => (`<pre>${JSON.stringify(
      err, null, 2,
    )}</pre>`));
  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return (
        <div>Loading pet data...</div>
      );
    }
    const {
      animal, location, description, breed, age, xname, media,
    } = this.state;
    return (
      <div className="details">
        <PetImages media={media} />

        <div>
          <h1>{xname}</h1><span>{age}</span>
          <h2>{animal}, {breed}, {location}</h2>
          <p>{description}</p>
        </div>
      </div>
    );
  }
}

export default Details;

