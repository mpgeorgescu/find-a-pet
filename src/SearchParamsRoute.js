import React from 'react';
import { navigate } from '@reach/router';
import SearchBox from './SearchBox';

class SearchParamsRoute extends React.Component {
  static handleSearchSubmit() {
    navigate('/');
  }

  render() {
    return (
      <div className="search-route">
        <SearchBox search={this.handleSearchSubmit} />
      </div>
    );
  }
}

export default SearchParamsRoute;
