import React from 'react';
import pf from 'petfinder-client';
import { Consumer } from './SearchContext';
import Pet from './Pet';
import SearchBox from './SearchBox';

const petfinder = pf({
  key: process.env.API_KEY,
  secret: process.env.API_SECRET,
});

class Results extends React.Component {
  constructor(props) {
    super(props);
    this.search = this.search.bind(this);
    this.state = {
      pets: [],
    };
  }

  /**
   * called after rendered on the DOM for the 1st time
   */
  componentDidMount() {
    this.search();
  }

  search() {
    const { searchParams } = this.props;
    petfinder.pet.find({
      output: 'full',
      location: searchParams.location,
      animal: searchParams.animal,
      breed: searchParams.breed,
    }).then((data) => {
      let pets;
      if (data.petfinder.pets && data.petfinder.pets.pet) {
        if (Array.isArray(data.petfinder.pets.pet)) {
          pets = data.petfinder.pets.pet;
        } else {
          pets = [data.petfinder.pets.pet];
        }
      } else {
        pets = [];
      }
      this.setState({
        pets,
      });
    });
  }

  render() {
    const { pets } = this.state;
    return (
      <div className="search">
        <SearchBox search={this.search} />
        {
          pets.map((pet) => {
            let { breed } = pet.breeds;
            if (Array.isArray(pet.breeds.breed)) {
              breed = pet.breeds.breed.join('/');
            }
            return (
              <Pet
                key={pet.id}
                id={pet.id}
                animal={pet.animal}
                xname={pet.name}
                breed={breed}
                media={pet.media}
                location={`${pet.contact.city}, ${pet.contact.state}`}
              />
            );
          })
        }
      </div>
    );
  }
}

export default function ResultsWithContext(props) {
  return (
    <Consumer>
      {
        context => <Results {...props} searchParams={context} />
      }
    </Consumer>
  );
}

