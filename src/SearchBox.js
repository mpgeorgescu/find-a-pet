import React from 'react';
import { ANIMALS } from 'petfinder-client';
import { Consumer } from './SearchContext';

class SearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleFormSubmit(event) {
    event.preventDefault();
    const { search } = this.props;
    search();
  }

  render() {
    return (
      <Consumer>
        {context => (
          <div className="search-params">
            <form onSubmit={this.handleFormSubmit}>

              <label htmlFor="location">
                Location
                <input
                  id="location"
                  value={context.location}
                  placeholder="Location"
                  onChange={context.handleLocationChange}
                />
              </label>

              <label htmlFor="animal">
                Animal
                <select
                  id="animal"
                  value={context.animal}
                  placeholder="Animal"
                  onChange={context.handleAnimalChange}
                  onBlur={context.handleAnimalChange}
                >
                  <option />
                  {
                    ANIMALS.map(a => (
                      <option key={a} value={a}>{a}</option>
                    ))
                  }
                </select>
              </label>

              <label htmlFor="breed">
                Breed
                <select
                  id="breed"
                  disabled={!context.breeds.length}
                  value={context.breed}
                  placeholder="Breed"
                  onChange={context.handleBreedChange}
                  onBlur={context.handleBreedChange}
                >
                  <option />
                  {
                    context.breeds.map(breed => (
                      <option key={breed} value={breed}>{breed}</option>
                    ))
                  }
                </select>
              </label>
              <button type="submit">Search</button>

            </form>
          </div>
        )}
      </Consumer>
    );
  }
}

export default SearchBox;
