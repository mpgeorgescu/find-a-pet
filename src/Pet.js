import React from 'react';
import { Link } from '@reach/router';

class Pet extends React.Component {
  render() {
    const {
      xname, animal, breed, media, location, id,
    } = this.props;
    let photos = ['unknown'];
    if ((media && media.photos && media.photos.photo)) {
      const pntPhotos = media.photos.photo.filter(photo => photo['@size'] === 'pnt');
      if (Array.isArray(pntPhotos) && pntPhotos.length) {
        photos = pntPhotos;
      }
    }

    return (
      <Link to={`/details/${id}`} className="pet">
        <div className="image-container">
          <img alt={xname} src={photos && photos[0] ? photos[0].value : ''} />
        </div>
        <div className="info">
          <h1>{xname}</h1>
          <h2>
            {animal} {breed} {location}
          </h2>
        </div>
      </Link>
    );
  }
}
export default Pet;
