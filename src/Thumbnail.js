import React from 'react';

class Thumbnail extends React.Component {
  render() {
    const {
      photo, index, active, handleThumbNailClick,
    } = this.props;

    return (
      /* eslint-disable-next-line */
      <img
        onClick={handleThumbNailClick}
        alt={`pet thumbnail ${index + 1}`}
        src={photo.value}
        className={index === active ? 'active' : ''}
        data-index={index}
      />
    );
  }
}

export default Thumbnail;
