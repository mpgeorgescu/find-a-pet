import React from 'react';

// create provider & consumer components:
const SearchContext = React.createContext({
  location: process.env.MY_LOCATION,
  animal: '',
  breeeds: [],
  handleAnimalChange() {},
  handleBreedChange() {},
  handleLocationChange() {},
  getBreeds() {},
});

export const { Provider, Consumer } = SearchContext;
